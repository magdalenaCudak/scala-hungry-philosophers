import Fork.{AreYouBusy, YouAreFree}
import Philosopher._
import Table._
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, PoisonPill, Props}

/**
  * Table coordinates communication between Forks and Philosophers
  * @param philosophers: Array[Philosopher]
  * @param forks: Array[Fork]
  */
class Table(var philosophers: Array[ActorRef], var forks: Array[ActorRef]) extends Actor{
  def receive: Receive = {
    case AddPhilosopher(philosopher, fork) =>
      philosophers = philosophers :+ philosopher
      forks = forks :+ fork
    case MakePhilosopherHungry(id) =>
      philosophers(id) ! Eat
    case IAmHungry(id) =>
      forks(id) ! AreYouBusy(id) // left fork
      forks((id+1)%forks.length) ! AreYouBusy(id) //right fork
    case IAmBusy(forkId, philosopherId) =>
      if (forkId == philosopherId)
        philosophers(philosopherId) ! LeftForkBusy
      else
        philosophers(philosopherId) ! RightForkBusy
    case IAmFree(forkId, philosopherId) =>
      if (forkId == philosopherId)
        philosophers(philosopherId) ! LeftForkFree
      else
        philosophers(philosopherId) ! RightForkFree
    case PutDown(id) =>
      forks(id % forks.length) ! YouAreFree
    case KillAllPlease =>
      for (i <- 0 until philosophers.length){
        philosophers(i) ! PoisonPill
      }
      for (i <- 0 until forks.length){
        forks(i) ! PoisonPill
      }
  }

  def makePhilosopherHungry(id: Int): Unit ={
    philosophers(id) ! Eat
  }
}


object Table {
  def props ( philosophers: Array[ActorRef], forks: Array[ActorRef]): Props = {
    Props(new Table(philosophers, forks))
  }

  final case class IAmHungry(id: Int)
  final case class IAmBusy(forkId: Int, philosopherId: Int)
  final case class IAmFree(forkId: Int, philosopherId: Int)
  final case class PutDown(id: Int)
  final case class  AddPhilosopher(philosopher: ActorRef, fork: ActorRef)
  final case class  MakePhilosopherHungry(id: Int)
  case object KillAllPlease
}
