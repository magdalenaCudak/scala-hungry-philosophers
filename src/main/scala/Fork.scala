
import Fork.{AreYouBusy, YouAreFree}
import Table.{IAmBusy, IAmFree}
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}

/**
  * The fork exists to be used by Philosopher
  * It sends info about it's status
  *
  * @param id   : Int
  * @param busy : Boolean
  */
class Fork(val id: Int, val table: ActorRef, var busy: Boolean) extends Actor {

  def receive: Receive = {
    case AreYouBusy(askingPhilosopher) =>
      if (busy) {
        table ! IAmBusy(id, askingPhilosopher)
      } else {
        table ! IAmFree(id, askingPhilosopher)
        busy = true
      }
    case YouAreFree =>
      busy = false
  }
}

object Fork {
  def props(id: Int, table: ActorRef): Props = {
    Props(new Fork(id, table, false))
  }


  final case class AreYouBusy(askingPhilosopher: Int)
  case object YouAreFree

}