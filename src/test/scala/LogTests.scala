import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{EventFilter, ImplicitSender, TestActorRef, TestActors, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, Matchers, MustMatchers, WordSpecLike}
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import Table._
import Fork._
import Philosopher._

class LogTests extends TestKit(ActorSystem("helloAkka", ConfigFactory.parseString("""akka.loggers = ["akka.testkit.TestEventListener"]""")))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  "verify if Philosopher is eating" in {
    val table = TestProbe()
    val platon = system.actorOf(Philosopher.props(0, table.ref, PhilosopherState.Thinking))
    EventFilter.info(pattern = "Philosopher 0 is eating", occurrences = 1) intercept {
      platon ! Eat
      table.expectMsg(500 millis, IAmHungry(0))
      platon ! LeftForkFree
      platon ! RightForkFree

    }
  }

  "verify if Philosopher is eating double" in {
    val table = TestProbe()
    val platon = system.actorOf(Philosopher.props(0, table.ref, PhilosopherState.Thinking))
    EventFilter.info(pattern = "Philosopher 0 is eating", occurrences = 2) intercept {
      platon ! Eat
      platon ! Eat
      table.expectMsg(500 millis, IAmHungry(0))
      platon ! LeftForkFree
      platon ! RightForkFree

      table.expectMsg(1500 millis, PutDown(0))
      table.expectMsg(1500 millis, PutDown(1))
      table.expectMsg(1500 millis, IAmHungry(0))
      platon ! LeftForkFree
      platon ! RightForkFree
    }
  }

  "verify if Philosopher is eating expected times" in {
    val number_of_philosophers = 5

    var table: ActorRef = system.actorOf(Table.props(new Array[ActorRef](0), new Array[ActorRef](0)), "table")
    for (i <- 0 until number_of_philosophers) {
      val arystoteles: ActorRef = system.actorOf(Philosopher.props(i, table, PhilosopherState.Thinking), ("Arystoteles" + i))
      val fork: ActorRef = system.actorOf(Fork.props(i, table), ("fork" + i))
      table ! AddPhilosopher(arystoteles, fork)
    }
    EventFilter.info(pattern = "Philosopher 0 is eating", occurrences = 5) intercept {
      for (j <- 0 until 5) {
        for (i <- 0 until number_of_philosophers) {
          table ! MakePhilosopherHungry(i)
        }
        Thread.sleep(500)
      }
      Thread.sleep(10000)
    }

  }

}